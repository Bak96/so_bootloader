org 0x7c00

jmp 0:start

start:
	mov ax, cs
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov sp, 0x8000
	
	push dx

	mov bx, ENTER_MSG
	call print_string
	
	call read_name
	
	mov bx, WELCOME_MSG
	call print_string
	mov bx, BUFFER
	call print_string
	mov bx, NEW_LINE
	call print_string
	
	call sleep2s
	
	pop dx
	jmp load_second_part	

sleep2s:
	mov ah, 0x86
	mov cx, 0x1e
	mov dx, 0x8480
	int 0x15
	ret	
;bx begin

read_input:
	mov ah, 0x0
	int 0x16
	ret
read_name:
	mov bx, 0
read_name_loop:
	call read_input
	cmp al, 0x61	
	jae check_az_range	;sprawdzamy czy liczba z zakresu a-z
check_backspace:
	cmp al, 0x08
	je backspace_pressed
check_enter:
	cmp al, 0x0d
	je enter_pressed
	jmp read_name_loop
	
backspace_pressed:
	cmp bx, 0
	je read_name_loop
	dec bx
	call print_char
	mov al, 0
	call print_char
	mov al, 0x08
	call print_char
	jmp read_name_loop

enter_pressed:
	cmp bx, 3
	jb read_name_loop
	mov [BUFFER + bx], byte 0x0
	call save_name_on_disk
	ret
	
check_az_range:
	cmp al, 0x7a
	jbe print_char_check
	jmp check_backspace
	
;al register
print_char_check:
	cmp bx, 12	;czy mozna wypisac
	je read_name_loop		;nie mozna
	mov byte[BUFFER + bx], al
	inc bx
	call print_char
	jmp read_name_loop

print_char:
	mov ah, 0xe
	int 0x10
	ret
	
print_string:
	mov al, byte [bx]
	cmp al, 0
	je end_print_string
	call print_char
	inc bx
	jmp print_string
end_print_string:
	ret	
load_second_part:
	mov ah, 0x02	;read sector
	mov al, 1	;sectors to read count
	mov ch, 0	;cylinder
	mov cl, 2	;sector
	mov dh, 0	;head
;	mov dl, 0x80	;drive
	mov bx, start_minix;	buffer address pointer	
	int 0x13
	jmp bx	

save_name_on_disk:
	mov ah, 0x03	;write sector to drive
	mov al, 1		;sectors to write count
	mov ch, 0		;track
	mov cl, 4		;sector
	mov dh, 0		;head
	mov dl, 0x80
	mov bx, BUFFER
	int 0x13
	ret
	
ENTER_MSG: db 'Enter your name', 0xd, 0xa, 0x0
WELCOME_MSG: db 0xd, 0xa, 'Hello ', 0x0
NEW_LINE: db 0xd, 0xa, 0x0
BUFFER times 13 db 0

times (510-$+$$) db 0
dw 0xaa55

start_minix:
	mov ah, 0x02	;read sector
	mov al, 1	;sectors to read count
	mov ch, 0	;cylinder
	mov cl, 3	;sector
	mov dh, 0	;head
;	mov dl, 0x80	;drive
	mov bx, 0x7c00	;buffer address pointer	
	int 0x13
	jmp bx
