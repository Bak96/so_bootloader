#so_bootloader

A program that is being loaded instead of original bootloader.
Program asks user for a name and stores it on a hard disk.
Next program loads original bootloader to a memory and runs it.
After operating system is started, system automatically creates a user with the name stored on a disk and logs in as this user.

Technology: Assembly, Minix


